class Item:
	importedTag= "imported"

	def __init__(self, string):
		self.tax1= True
		self.tax2= False
		self.taxAmount= 0
	
		elements= string.split(" ", 1)
		self.amount= int(elements[0])
		rest= elements[1]
		
		elements= rest.split(" at ", 1)
		
		desc= elements[0]
		if Item.importedTag in desc:
			self.tax2= True
			s= desc.replace(Item.importedTag+" ", "")
			desc= Item.importedTag+" "+s			
			
		self.description= desc
		self.price= float(elements[1])
		
class Categorizer:
	tax1ExcludedNames= ["book", "chocolate", "headache pill"]
	
	@staticmethod
	def categorize(itemList):
		for item in itemList:
			Categorizer._checkCategory(item)
	
	@staticmethod
	def _checkCategory(item):
		for n in Categorizer.tax1ExcludedNames:
			if n in item.description:
				item.tax1= False

			
class Taxer:
	tax1Perc= 10
	tax2Perc= 5
	
	@staticmethod
	def tax(itemList):
		for item in itemList:
			Taxer._taxItem(item)
	
	@staticmethod
	def _taxItem(item):
		tax= 0
		
		if item.tax1:
			tax += Taxer._getTaxFromPerc(item.price, Taxer.tax1Perc)
		if item.tax2:
			tax += Taxer._getTaxFromPerc(item.price, Taxer.tax2Perc)
		
		item.taxAmount= tax
	
	@staticmethod
	def _getTaxFromPerc(price, perc):
		tax= price*perc / 100		
		tax= Taxer._round(tax)
		
		return tax
		
	@staticmethod
	def _round(tax):
	
		ris= int(tax*10) / 10	
		dec= int(tax*100%10)
		
		if dec >5:
			dec= 10
		dec /= 100		
		ris += dec
		
		return ris
		
class Printer:
	@staticmethod
	def getOutput(itemList):
		output= ""
		for item in itemList:
			output += Printer._printItem(item) + "\n"
			
		prices= [item.price for item in itemList]
		taxes= [item.taxAmount for item in itemList]
		output += "Sales Taxes: " + Printer._getStringWithZeros(sum(taxes)) + "\n"
		output += "Total: " + Printer._getStringWithZeros(sum(prices)+sum(taxes)) + "\n"
		
		return output
			
	@staticmethod
	def _printItem(item):
		return str(item.amount)+" "+item.description+": " + Printer._getStringWithZeros(item.price+item.taxAmount)
		
	@staticmethod
	def _getStringWithZeros(price):
		return format(price, '.2f')


class SalesTaxesCalculator:
	def __init__(self):
		self.input= []
		self.output= []
		
	def setInput(self, input):
		self.input= []
		for e in input:
			self.input.append(Item(e))
		Categorizer.categorize(self.input)

	def calculate(self):
		Taxer.tax(self.input)
		return Printer.getOutput(self.input)


if __name__=="__main__":
	input1= []
	input1.append("1 book at 12.49")
	input1.append("1 music CD at 14.99")
	input1.append("1 chocolate bar at 0.85")
	output1= "1 book: 12.49\n1 music CD: 16.49\n1 chocolate bar: 0.85\nSales Taxes: 1.50\nTotal: 29.83\n"
	
	input2= []
	input2.append("1 imported box of chocolates at 10.00")
	input2.append("1 imported bottle of perfume at 47.50")
	output2= "1 imported box of chocolates: 10.50\n1 imported bottle of perfume: 54.65\nSales Taxes: 7.65\nTotal: 65.15\n"
	
	input3= []
	input3.append("1 imported bottle of perfume at 27.99")
	input3.append("1 bottle of perfume at 18.99")
	input3.append("1 packet of headache pills at 9.75")
	input3.append("1 box of imported chocolates at 11.25")
	output3= "1 imported bottle of perfume: 32.19\n1 bottle of perfume: 20.89\n1 packet of headache pills: 9.75\n1 imported box of chocolates: 11.85\nSales Taxes: 6.70\nTotal: 74.68\n"
	
	calculator= SalesTaxesCalculator()

	calculator.setInput(input1)
	output= calculator.calculate()
	print(output)
	assert output==output1
	
	calculator.setInput(input2)
	output= calculator.calculate()
	print(output)
	assert output==output2

	calculator.setInput(input3)
	output= calculator.calculate()
	print(output)
	assert output==output3
